#pragma once
#include <QVector>
#include <QVariantList>
#include "include/can/CANDisplay.h"
#include "include/can/MsgProperties.h"
class MsgGroup {
public:
	// Init each message based on db property values
	void addMessage(QVariantList properties);

	QVector<MsgProperties> idMessages;
};
