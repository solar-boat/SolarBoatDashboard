#pragma once
enum class DataType {
		FLOAT,
		SIGNED,
		UNSIGNED,
		UNDEFINED
};
