#pragma once
#include "include/can/MsgGroup.h"
#include "include/can/CANDisplay.h"
#include <QMap>
#include <string>
#include "include/can/DataType.h"

//TODO: Define a constant for the number of bits in transmission. I am doing a
// TERRIBLE thing using hard-coded sizes for transfer, but I can't figure out how
// to translate CAN data otherwise. May turn the values into constants later
class CANInterpreter {
public:
	CANInterpreter();
	static void parseFrame(can_frame frame);
	// Helper functions
	static uint8_t *getData(int start_bit, int num_bits, uint8_t data[8], uint8_t dataLen);
	static QVariant formatData(uint8_t *msgData, int num_bits, DataType data_type);
	static void assignUi(CANDisplay *ui);

	static QMap<uint32_t, MsgGroup> messages;
private:
	static CANDisplay *ui;
	static QVector<void *> batchNotifies;
};
