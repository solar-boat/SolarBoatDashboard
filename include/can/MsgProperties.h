#pragma once
#include <string>
#include "include/can/DataType.h"

class MsgProperties {
public:
		std::string name;
		int start_bit;
		int num_bits;
        DataType data_type;
};
