#pragma once
#include <QObject>
#include <QVariant>
#include <QVector>
#include <QTimer>
#include <QMap>

#include "include/can/canBus.h"
#include "include/can/CANInterface.h"
#include "include/can/canSocket.h"


class CANDisplay : public QObject {
	Q_OBJECT

	Q_PROPERTY(unsigned int chargeState READ getChargeState WRITE setChargeState NOTIFY chargeStateChanged)
	Q_PROPERTY(unsigned int ampHours READ getAmpHours WRITE setAmpHours NOTIFY ampHoursChanged)
	Q_PROPERTY(unsigned int voltage READ getVoltage WRITE setVoltage NOTIFY voltageChanged)
	Q_PROPERTY(int current READ getCurrent WRITE setCurrent NOTIFY currentChanged)
	Q_PROPERTY(unsigned int bmsStatus READ getBmsStatus WRITE setBmsStatus NOTIFY bmsStatusChanged)
	Q_PROPERTY(unsigned int avgPackTemp READ getAvgPackTemp WRITE setAvgPackTemp NOTIFY avgPackTempChanged)
	Q_PROPERTY(unsigned int highTemp READ getHighTemp WRITE setHighTemp NOTIFY highTempChanged)
	Q_PROPERTY(unsigned int cycleCount READ getCycleCount WRITE setCycleCount NOTIFY cycleCountChanged)
public:
	CANDisplay(QObject *parent = 0);

	unsigned int getChargeState();
	void setChargeState(QVariant chargeState);

	unsigned int getAmpHours();
	void setAmpHours(QVariant ampHours);

	unsigned int getVoltage();
	void setVoltage(QVariant voltage);

	int getCurrent();
	void setCurrent(QVariant current);

	unsigned int getBmsStatus();
	void setBmsStatus(QVariant bmsStatus);

	unsigned int getAvgPackTemp();
	void setAvgPackTemp(QVariant avgPackTemp);

	unsigned int getHighTemp();
	void setHighTemp(QVariant highTemp);

	unsigned int getCycleCount();
	void setCycleCount(QVariant cycleCount);

	void updateUI(QVariantList newVals);

private:
	unsigned int chargeState;
	unsigned int ampHours;
	unsigned int voltage;
	int current;
	unsigned int bmsStatus;
	unsigned int avgPackTemp;
	unsigned int highTemp;
	unsigned int cycleCount;

	// Places each emit in the function vector
	void populateFuncLists();

	const int UPDATE_INTERVAL = 2000; // in ms

	static CANInterface *canInterface;

	QTimer *updater;
	// Keep track of all variable notifies. This is initialized in the constructor
	QVector<std::function<void()>> changeSignals;
	QMap<std::string, std::function<void(QVariant)>> setChangedVars;

signals:
	void chargeStateChanged();
	void ampHoursChanged();
	void voltageChanged();
	void currentChanged();
	void bmsStatusChanged();
	void avgPackTempChanged();
	void highTempChanged();
	void cycleCountChanged();


public slots:
	// Communicate with QML interface
	void updateCanVals();
};
