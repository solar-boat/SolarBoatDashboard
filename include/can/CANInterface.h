#pragma once

#include <QString>
#include <QDebug>
#include <cmath>
#include <functional>
#include "canBus.h"
#include "canSocket.h"

class CANInterface {
public:
		explicit CANInterface();
		~CANInterface();
    bool startListening(); //Start listening to the activity on the CAN bus.
    void stopListening();  //Stop listening to the CAN bus.
		bool writeCANFrame(int ID, QByteArray payload);

    void readFrame(can_frame frame);

private:
    CanBusModule canBus;
    bool slcandActive;
    bool activateSlcand();
    bool disableSlcand();
};
