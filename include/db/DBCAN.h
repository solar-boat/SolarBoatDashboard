#pragma once
#include <QString>
#include <QMap>
#include <QSqlQuery>
#include <stdexcept>
#include "include/can/MsgGroup.h"


class DBCAN {
public:
    QMap<uint32_t, MsgGroup> createMsgMap();
	MsgGroup getMsgGroup(int id);

private:
    const QString TABLE = "`can_msg_properties`";
    const QString readEntriesByIdStr = "SELECT * FROM " + DBCAN::TABLE + " WHERE id = :id";
    const QString readIdsStr = "SELECT DISTINCT `id` FROM " + DBCAN::TABLE;
};
