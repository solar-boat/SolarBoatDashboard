#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

//Local includes
#include "include/db/DBConnection.h"
#include "include/db/DBMarker.h"
#include "include/db/DBPath.h"
#include "include/qml_classes/RaceTypeClass.h"
#if defined(Q_OS_ANDROID) || defined(Q_OS_LINUX)
#include "include/can/CANDisplay.h"
#include "include/can/CANInterface.h"
#include "include/db/DBCAN.h"
#include "include/can/CANInterpreter.h"
#include "include/can/CANDisplay.h"
#endif

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;

		CANInterface *can = new CANInterface();

    DBPath * dbMarkerPath = new DBPath();
    DBMarker *dbMarker = new DBMarker();

    CANDisplay *canDisplay = new CANDisplay();
    //		can_frame frame;
    //		uint8_t test = {31, 50, 43, 52, 65, 32, 1, 34};
    //		frame.data = test;
    //		frame.can_id = 1030;
    //		frame.can_dlc = 8;
    //		CANInterpreter::parseFrame(frame);

    // Allow QML to access necessary C++ classes
    engine.rootContext()->setContextProperty("DBPath", dbMarkerPath);
    engine.rootContext()->setContextProperty("DBMarker", dbMarker);
    engine.rootContext()->setContextProperty("CANDisplay", canDisplay);

    // Allow QML to access class that interfaces with CAN data
    //    engine.rootContext()->setContextProperty("CANDisplay", canDisplay);

    // Create a database connection
    DBConnection *dbcon = new DBConnection();
    dbcon->initConnection();

    DBCAN *dbCan = new DBCAN();
    CANInterpreter::messages = dbCan->createMsgMap();
    CANInterpreter::assignUi(canDisplay);

    // Allow race type enum in qml
    RaceTypeClass::init();

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    if (engine.rootObjects().isEmpty())
        return -1;

        can->startListening();

    return app.exec();
}
