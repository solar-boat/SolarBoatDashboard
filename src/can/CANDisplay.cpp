#include "include/can/CANDisplay.h"
#include "include/can/CANInterpreter.h"

#include <functional>
#include <QList>

CANDisplay::CANDisplay(QObject *parent) :	QObject(parent) {
	this->populateFuncLists();

	updater = new QTimer();
	connect(updater, SIGNAL(timeout()), this, SLOT(updateCanVals()));
    setAmpHours(34);
    ampHoursChanged();
	updater->start(UPDATE_INTERVAL);
}

unsigned int CANDisplay::getChargeState() {
	return this->chargeState;
}
void CANDisplay::setChargeState(QVariant chargeState) {
	this->chargeState = chargeState.value<uint16_t>();
}

unsigned int CANDisplay::getAmpHours() {
	return this->ampHours;
}
void CANDisplay::setAmpHours(QVariant ampHours) {
	this->ampHours = ampHours.value<uint16_t>();
}

unsigned int CANDisplay::getVoltage() {
	return this->voltage;
}
void CANDisplay::setVoltage(QVariant voltage) {
	this->voltage = voltage.value<uint16_t>();
}

int CANDisplay::getCurrent() {
	return this->current;
}
void CANDisplay::setCurrent(QVariant current) {
    qDebug() << "Current current: " << current << "\n";
	this->current = current.value<int16_t>();
}

unsigned int CANDisplay::getBmsStatus() {
	return this->bmsStatus;
}
void CANDisplay::setBmsStatus(QVariant bmsStatus) {
	this->bmsStatus = bmsStatus.value<uint16_t>();
}

unsigned int CANDisplay::getAvgPackTemp() {
	return this->avgPackTemp;
}
void CANDisplay::setAvgPackTemp(QVariant avgPackTemp) {
	this->avgPackTemp = avgPackTemp.value<uint16_t>();
}

unsigned int CANDisplay::getHighTemp() {
	return this->highTemp;
}
void CANDisplay::setHighTemp(QVariant highTemp) {
	this->highTemp = highTemp.value<uint16_t>();
}

unsigned int CANDisplay::getCycleCount() {
	return this->cycleCount;
}
void CANDisplay::setCycleCount(QVariant cycleCount) {
	this->cycleCount = cycleCount.value<uint16_t>();
}

void CANDisplay::updateCanVals() {
    qDebug() << "Calling a notify function.\n";
	for (std::function<void()> fn : changeSignals) {
        qDebug() << "Notify function directly.\n";
        fn();
	}

}

void CANDisplay::populateFuncLists() {
	// Creates a vector of emit functions that can be called every timout
	changeSignals.push_back(std::bind(&CANDisplay::chargeStateChanged, this));
	changeSignals.push_back(std::bind(&CANDisplay::ampHoursChanged, this));
	changeSignals.push_back(std::bind(&CANDisplay::voltageChanged, this));
	changeSignals.push_back(std::bind(&CANDisplay::currentChanged, this));
	changeSignals.push_back(std::bind(&CANDisplay::bmsStatusChanged, this));
	changeSignals.push_back(std::bind(&CANDisplay::avgPackTempChanged, this));
	changeSignals.push_back(std::bind(&CANDisplay::highTempChanged, this));
	changeSignals.push_back(std::bind(&CANDisplay::cycleCountChanged, this));

	setChangedVars.insert("ChargeState", std::bind(&CANDisplay::setChargeState, this, std::placeholders::_1));
	setChangedVars.insert("AmpHours"   , std::bind(&CANDisplay::setAmpHours   , this, std::placeholders::_1));
	setChangedVars.insert("Voltage"    , std::bind(&CANDisplay::setVoltage    , this, std::placeholders::_1));
	setChangedVars.insert("Current"    , std::bind(&CANDisplay::setCurrent    , this, std::placeholders::_1));
	setChangedVars.insert("BMS_Status" , std::bind(&CANDisplay::setBmsStatus  , this, std::placeholders::_1));
	setChangedVars.insert("AvgPackTemp", std::bind(&CANDisplay::setAvgPackTemp, this, std::placeholders::_1));
	setChangedVars.insert("HighTemp"   , std::bind(&CANDisplay::setHighTemp   , this, std::placeholders::_1));
	setChangedVars.insert("CycleCount" , std::bind(&CANDisplay::setCycleCount , this, std::placeholders::_1));
}

void CANDisplay::updateUI(QVariantList newVals) {
    qDebug() << "Calling the updateUI function\n";
	// List contains the data name and corresponding value. Parse and update
	// accordingly
	for (QVariant el : newVals) {
		QVariantList elList = el.toList();
		std::string key = elList.at(0).toString().toStdString();
        qDebug() << "updateUI key: " << QString(key.c_str()) << "\n";
		if (!setChangedVars.contains(key)) {
			throw std::runtime_error("updateUI: attempted to update a variable for which there was no matching"
															 "'set' function. Make sure setChangedVars contains each setter.");
		}
        qDebug() << "updateUI new value: " << elList.at(1) << "\n";
		setChangedVars[key](elList.at(1));
	}
}
