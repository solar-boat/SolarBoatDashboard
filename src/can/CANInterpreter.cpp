#include "include/can/CANInterpreter.h"
#include "include/can/MsgGroup.h"
#include "include/db/DBCAN.h"

#include <cstring>
#include <QVariantList>
#include <QVariant>
#include <QString>
QMap<uint32_t, MsgGroup> CANInterpreter::messages;
CANDisplay *CANInterpreter::ui;

void CANInterpreter::parseFrame(can_frame frame) {
	if (!messages.contains(frame.can_id)) {
		// We only want to parse messages we care about
        qDebug() << "Did not find message ID." << "\n";
		return;
	}
	MsgGroup group = CANInterpreter::messages[frame.can_id];
    qDebug() << "Got CAN frame ID: " << frame.can_id;
    qDebug() << "CAN DLC: " << frame.can_dlc;
	uint8_t *msgData;
	QVariantList formattedData;
	QVariantList pushToUi;
	for (MsgProperties props : group.idMessages) {
		try {
			msgData = CANInterpreter::getData(props.start_bit, props.num_bits, frame.data, frame.can_dlc);
			formattedData << QString::fromStdString(props.name);
			formattedData << CANInterpreter::formatData(msgData, props.num_bits, props.data_type);

			pushToUi.insert(pushToUi.size(), formattedData);
            formattedData.clear();
		} catch(std::runtime_error &e) {
            qDebug() << "Error happened: " << e.what();
			//TODO: flesh this out
		}
	}
	ui->updateUI(pushToUi);

}

uint8_t *CANInterpreter::getData(int start_bit, int num_bits, uint8_t data[8], uint8_t dataLen) {
	// At the moment, this function can only handle data packaged in 8-bit pieces minimum
	if (start_bit < 0) {
		throw std::runtime_error("getData: negative start index");
	}
    else if (start_bit + num_bits > dataLen * 8) {
		throw std::runtime_error("getData: copy range exceeds data length");
	}

	QVector<uint8_t> msgData;

	// If system is little endian, byte order is flipped. To account for this, add them in reverse order
    int startIdx = start_bit/8;
    int endIdx = (start_bit + num_bits)/8;
    for (int i = startIdx; i < endIdx; i++) {
        msgData.push_back(data[i]);
	}
	return msgData.data();
}

QVariant CANInterpreter::formatData(uint8_t *msgData, int num_bits, DataType data_type) {
    qDebug() << "Entered formatData\n";
    QString debugging = "";
	switch(data_type) {
	case DataType::FLOAT:
		if (num_bits != 32) {
			std::string errMsg = "formatData error: attempted to create a float with"
					+ num_bits + std::string(" bits instead of 32");
			throw std::runtime_error(errMsg);
		}
		union {
			float f;
			uint8_t bytes[sizeof(float)];
		} u;
		memcpy(u.bytes, msgData, sizeof(float));
		return u.f;
		break;
	case DataType::UNSIGNED:
		switch(num_bits) {
		case 8:
			// No manipulation needed
			return *msgData;
			break;
		case 16:
			// Concatenate bytes into larger number
			return *reinterpret_cast<uint16_t*>(&msgData);
			break;
		default:
			throw std::runtime_error("formatData error: unsupported unsigned type cast");
		}
	case DataType::SIGNED:
        for(int i = 0; i < 4; i++)
        {
            debugging += QString("%1").arg(msgData[i] , 0, 16);
        }
        qDebug() << "MsgData FormatData: " << debugging << "\n";
		switch(num_bits) {
		case 8:
			return *reinterpret_cast<int8_t*>(&msgData);
			break;
        case 16:
            qDebug() << "Current value after conversion: " << *reinterpret_cast<int16_t*>(&msgData) << "\n";
			return *reinterpret_cast<int16_t*>(&msgData);
			break;
		default:
			throw std::runtime_error("formatData error: unsupported signed type cast");
			break;
		}
	case DataType::UNDEFINED:
		throw std::runtime_error("formatData error: UNDEFINED data type encountered");
		break;
	default:
		QString errorMessage = "formatData error: Invalid data type";
		throw std::runtime_error(errorMessage.toStdString());
	}
}

void CANInterpreter::assignUi(CANDisplay *ui) {
	CANInterpreter::ui = ui;
}


