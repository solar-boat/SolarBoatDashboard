#include "include/can/MsgGroup.h"

void MsgGroup::addMessage(QVariantList properties) {
	MsgProperties tmp;
	tmp.name = properties.takeFirst().toString().toStdString();
	tmp.start_bit = properties.takeFirst().toInt();
	tmp.num_bits = properties.takeFirst().toInt();
	DataType type;
	QString propType = properties.takeFirst().toString();
	if (propType == "Float") {
		type = DataType::FLOAT;
	}
	else if (propType == "Unsigned") {
		type = DataType::UNSIGNED;
	}
	else if (propType == "Signed") {
		type = DataType::SIGNED;
	}
	else {
		type = DataType::UNDEFINED;
	}
	tmp.data_type = type;
	idMessages.push_back(tmp);
}
