﻿#include "include/db/DBCAN.h"
#include <QDebug>
#include <QSqlError>
#include <QVariantList>

QMap<uint32_t, MsgGroup> DBCAN::createMsgMap() {
    QMap<uint32_t, MsgGroup> map;
    QSqlQuery q;
    uint32_t id;
    if (q.exec(readIdsStr)) {
        while (q.next()) {
            // Create map entry for each id
            id = q.value(0).toUInt();
            map.insert(id, DBCAN::getMsgGroup(id));
        }
        return map;
    } else {
        QString errorMessage = "populateMessages error: " + q.lastError().text();
        qDebug() << errorMessage;
        throw std::runtime_error(errorMessage.toStdString());
    }
}


MsgGroup DBCAN::getMsgGroup(int id) {
	MsgGroup group;
	QVariantList properties;
	QSqlQuery q;
	q.prepare(readEntriesByIdStr);
	q.addBindValue(id);

	if (q.exec()) {
		while (q.next()) {
			properties << q.value(1).toString(); // name
			properties << q.value(2).toInt(); // start_bit
			properties << q.value(3).toInt(); // num_bits
			properties << q.value(4).toString(); // data_type
			group.addMessage(properties);
			properties.clear();
		}
		// Now the group should contain all messages that came from a specific id.
		// Add these messages to message map
		return group;
	} else {
		QString errorMessage = "getMsgGroup error: " + q.lastError().text();
		qDebug() << errorMessage;
		throw std::runtime_error(errorMessage.toStdString());
	}
}
