#include "include/db/DBConnection.h"


DBConnection::DBConnection() {
}

void DBConnection::initConnection() {
    QString dbFile = ":/res/database/solar_boat.db";
    // Android doesn't allow file modification unless it is a local file
    QString newPath = QDir::currentPath() + "/test";
    QString newFile = newPath + "/solar_boat.db";
#ifdef Q_OS_ANDROID
    newPath = "/sdcard/solarBoat";
    newFile = newPath + "/solar_boat.db";
#endif
    if(QFile::exists(dbFile)) {
        QDir dir = QDir::root();
        dir.mkpath(newPath);
        QFile::copy(dbFile, newFile);
        QFile tempFile(dbFile);
        if(tempFile.exists())
        {
            QString tempStr = tempFile.readAll();
            int temp = 12;
        }
        QFile::setPermissions(dbFile,
          QFile::ReadOther|QFile::WriteOther);
        QFile::setPermissions(newFile,
          QFile::ReadOther|QFile::WriteOther);
    }
    database = QSqlDatabase::addDatabase("QSQLITE");
    database.setDatabaseName(newFile);
    if (!database.open()) {
        qDebug() << "Error: " << database.lastError().text();
    } else {
        qDebug() << "Successful conenction to " << newPath << endl;
    }
}

void DBConnection::closeConnection() {
    database.close();
}

bool DBConnection::isConnected() {
    return database.isOpen();
}
